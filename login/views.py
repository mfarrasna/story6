from django.shortcuts import render, redirect, reverse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from .forms import LoginForm

# Create your views here.
def login_user(request):

    if(request.method == 'POST'):
        loginform = LoginForm(request.POST)
        user = authenticate(username = request.POST['username'],password = request.POST['password'])

        if (user is not None):
            login(request, user)
            return redirect('/')
        else:
            return redirect('/login')
    
    else:
        loginform = LoginForm()
    return render(request, 'login.html', {'form' : loginform})


def logout_user(request):
    logout(request)
    return redirect('/')

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
    else:
        form = UserCreationForm()
    return render(request, 'register.html', {'form':form})