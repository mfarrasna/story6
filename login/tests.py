from django.test import TestCase

# Create your tests here.
from django.test import TestCase,Client

# Create your tests here.
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone

from .views import login_user,logout_user,register
from story6.settings import BASE_DIR

from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import unittest,time
import os   

class LoginTest(TestCase):
    def test_apakah_login_page_ada(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_jika_landing_page_tidak_ada(self):
        response = Client().get('/otherurl')
        self.assertEqual(response.status_code, 404)

    # def test_fungsi_login_page(self):
    #     found = resolve('/login')
    #     self.assertEqual(found.func, login_user)
    
    def test_apakah_register_page_ada(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)
    
class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=chrome_options)
        super(FunctionalTest,self).setUp()


    def test_login_after_register(self):
        driver = self.selenium
        driver.get('http://localhost:8000/')
        login_button = driver.find_element_by_id("login")
        login_button.click()
        time.sleep(2)
        username_textbox = driver.find_element_by_id("id_username")
        username_textbox.send_keys("mfarrasna")
        time.sleep(4)
        password_textbox = driver.find_element_by_id("id_password")
        password_textbox.send_keys("farras123")
        time.sleep(3)
        login = driver.find_element_by_id("login")
        login.click()
        time.sleep(2)
        self.assertIn("mfarrasna",driver.page_source)
        logout = driver.find_element_by_id("logout")
        logout.click()
        time.sleep(2)
        self.assertIn("Log in",driver.page_source)
        
    
    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()


    


