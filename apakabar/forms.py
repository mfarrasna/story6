from django import forms 
from .models import StatusModels
from datetime import datetime

class StatusForm(forms.ModelForm):
    class Meta:
        model = StatusModels
        fields = ['status']
        widgets= {
            'status': forms.Textarea(attrs={
            'class' : 'form-control',
            'type' : 'text',
            }),
        }
