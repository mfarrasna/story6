from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import home
from .models import StatusModels
from .forms import StatusForm
from selenium import webdriver

from django.test import LiveServerTestCase
import unittest, time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from datetime import date
import os
from story6.settings import BASE_DIR

# Create your tests here.
class UnitTest(TestCase):

    def test_landing_page_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_doesnt_exist(self):
        response = Client().get('/no_url/')
        self.assertEqual(response.status_code, 404)
    
    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_landing_page_exist(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
        self.assertContains(response, 'Halo, apakabar?')
    
    def test_status_models(self):
        status_test = StatusModels(
            status = "Sedihni",
            time = "2019-02-02 01:00:00",
        ) 
        status_test.save()
        self.assertEqual(StatusModels.objects.all().count(), 1)
    
    def test_status_form_is_valid(self):
        form_data = { 'status' : 'galaugaiz'}
        form = StatusForm(data = form_data)
        self.assertTrue(form.is_valid())
    
    # def test_apakah_login_page_ada(self):
    #     response = Client().get('/login')
    #     self.assertEqual(response.status_code, 200)
    
    # def test_apakah_register_page_ada(self):
    #     response = Client().get('/register')
    #     self.assertEqual(response.status_code, 200)

class FunctionalProjectTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=chrome_options)
        super(FunctionalProjectTest,self).setUp()

    def test_contains_status_with_functional_test(self):
        driver = self.selenium
        driver.get('http://localhost:8000')
        time.sleep(4)
        elem = driver.find_element_by_id("id_status")
        elem.send_keys("Coba Coba")
        time.sleep(4)   
        submit = driver.find_element_by_id("postbutton")
        submit.send_keys(Keys.RETURN)
        assert "Coba Coba" in driver.page_source
        time.sleep(5)


    def tearDown(self):
        self.selenium.quit()
        super(FunctionalProjectTest, self).tearDown()


    
    
    


    
    

    
    

    


