from django.shortcuts import render, redirect
from .models import StatusModels
from .forms import StatusForm


# Create your views here.
def home(request):
    semua_status = StatusModels.objects.all().order_by('-time')
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            hasil = form.save()
            return redirect("/")

    return render(request, 'index.html',{"form_now": StatusForm(), "semua_status": semua_status })
    