from django.db import models
from django.utils import timezone

# Create your models here.
class StatusModels(models.Model):
    status = models.CharField(max_length = 300)
    time = models.DateTimeField(auto_now = True)
